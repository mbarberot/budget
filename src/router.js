import Vue from 'vue'
import Router from 'vue-router'

import BudgetTabs from '@/components/budget/Tabs'
import Budget from '@/components/budget/Budget'
import Operations from '@/components/budget/operations/Operations'
import Overview from '@/components/budget/overview/Overview'
import Evolutions from '@/components/budget/evolutions/Evolutions'

Vue.use(Router)

const currentYear = `${new Date().getFullYear()}`
const currentMonth = `${new Date().getMonth()}`.padStart(2, 0)

export default new Router({
  routes: [{
    path: '/',
    name: 'index',
    redirect: `/budget/${currentYear}/${currentMonth}/operations`,
  }, {
    path: '/budget/:year?/:month?',
    name: 'budget',
    redirect: { name: 'operations' },
    components: {
      default: Budget,
      tabs: BudgetTabs,
    },
    children: [{
      path: 'operations',
      name: 'operations',
      component: Operations,
      props: true,
    }, {
      path: 'overview',
      name: 'overview',
      component: Overview,
      props: true,
    }, {
      path: 'evolutions',
      name: 'evolutions',
      component: Evolutions,
      props: true,
    }],
  }],
})
