import { mapMutations } from 'vuex'
import parseCSV from 'csv-parse/lib/sync'

const parsers = {
  json: (content) => JSON.parse(content),
  csv: (content) => parseCSV(content, { columns: true }),
}

export default {
  methods: {
    ...mapMutations({
      setOperations: 'operations/setOperations',
      addOperations: 'operations/addOperations',
    }),
    importFile(callback) {
      return (file) => {
        const extension = file.name.split('.').pop()
        const reader = new FileReader()
        reader.onload = event => {
          const content = event.currentTarget.result
          const operations = parsers[extension](content)
          callback(operations)
        }

        reader.readAsText(file, 'utf-8')
      }
    },
  },

  render() {
    return this.$scopedSlots.default({
      importOperations: this.importFile(this.setOperations),
      addOperations: this.importFile(this.addOperations),
    })
  },
}
