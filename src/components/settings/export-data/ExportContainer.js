import { mapGetters } from 'vuex'
import stringify from 'csv-stringify/lib/sync'

export default {
  props: {
    format: {
      type: String,
      required: true,
    },
  },
  computed: {
    ...mapGetters({
      operations: 'operations/getOperations',
    }),
    link () {
      let link
      switch (this.format) {
        case 'json':
          link = this.json()
          break
        case 'csv':
          link = this.csv()
          break
        default:
          link = ''
      }

      return link
    },
  },
  methods: {
    json () {
      return this.generateLink(JSON.stringify(this.operations))
    },
    csv () {
      return encodeURI(
        this.generateLink(stringify(this.operations, {
          quoted_string: true,
          header: true,
        }))
      )
    },
    generateLink (data) {
      return `data:text/plain;charset=utf-8,${data}`
    },
  },
  render () {
    return this.$scopedSlots.default({
      link: this.link,
    })
  },
}
