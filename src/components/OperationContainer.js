import { uniq } from 'lodash'
import addMonths from 'date-fns/addMonths'
import { mapGetters } from 'vuex'

const generateMonths = (firstMonth, lastMonth) => {
  const months = []
  let monthToPush = firstMonth
  do {
    months.push(monthToPush.toISOString().substring(0, 7))
    monthToPush = addMonths(monthToPush, 1)
  } while (monthToPush.getTime() < lastMonth.getTime())
  months.push(lastMonth.toISOString().substring(0, 7))
  return months;
}
export { generateMonths }

export default {

  computed: {
    ...mapGetters({
      operations: 'operations/getOperations',
    }),
    categories() {
      const categories = uniq(this.operations.map(({ category }) => category))
      categories.sort((a, b) => a.localeCompare(b))
      return categories
    },
    months() {
      const firstMonth = new Date(`${this.orderedOperations[this.orderedOperations.length - 1].date.substring(0, 7)}-05`)
      const lastMonth = new Date(`${this.orderedOperations[0].date.substring(0, 7)}-05`)

      return generateMonths(firstMonth, lastMonth)
    },
    operationsByMonths() {
      const byMonth = this.months.reduce((map, month) => {
        map[month] = []
        return map
      }, {})

      this.orderedOperations.forEach(operation => {
        const month = operation.date.substring(0, 7)
        byMonth[month] = [...byMonth[month] || [], operation]
      })

      return byMonth
    },
    orderedOperations() {
      const orderedOperations = [...this.operations]
      orderedOperations.sort(({ date: dateA }, { date: dateB }) => new Date(dateB).getTime() - new Date(dateA).getTime())
      return orderedOperations
    },
  },

  render() {
    return this.$scopedSlots.default({
      operations: this.orderedOperations,
      operationsByMonths: this.operationsByMonths,
      categories: this.categories,
      months: this.months,
      recettes: this.operations.filter(({ amount }) => amount >= 0),
      depenses: this.operations.filter(({ amount }) => amount < 0),
    })
  },
}
