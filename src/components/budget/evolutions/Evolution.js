import { Line } from 'vue-chartjs'

export default {
  extends: Line,
  props: {
    operations: {
      type: Object,
      required: true,
    },
    absolute: {
      type: Boolean,
      required: false,
      default: false,
    },
  },
  computed: {
    operationsByMonths() {
      return Object.entries(this.operations).reduce((byMonth, [month, operations]) => ({
         ...byMonth,
         [month]: Math.round(operations
            .map(({amount}) => Number(amount))
            .map(amount => (this.absolute ? Math.abs(amount) : amount))
            .reduce((sum, amount) => sum + amount, 0)),
      }), {});
    },
    labels() {
      const labels = Object.keys(this.operationsByMonths)
      return labels
    },
    values() {
      const values = Object.values(this.operationsByMonths)
      return values
    },
  },
  mounted() {
    const data = {
      datasets: [{
        data: this.values,
        backgroundColor: [
          'rgba(255, 159, 64, 0.7)',
        ],
        fill: true,
        lineTension: 0.4,
      }],
      labels: this.labels,
    }

    this.renderChart(data, {
      responsive: true,
      legend: {
        display: false,
      },
      scales: {
        yAxes: [{
          id: 'y-axis-id',
          type: 'linear',
          ticks: {
            beginAtZero: true,
          },
          position: 'left',
        }],
      },
    })
  },
}
