import { mapMutations } from 'vuex'

export default {
  props: {
    operation: {
      type: Object,
      required: true,
    },
  },
  methods: {
    ...mapMutations({
      updateOperation: 'operations/setOperation',
    }),
    updateCategory (category) {
      this.updateOperation({
        ...this.operation,
        category,
      })
    },
  },
  render () {
    return this.$scopedSlots.default({
      ...this.operation,
      updateCategory: this.updateCategory,

    })
  },
}
