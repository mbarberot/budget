import { Doughnut } from 'vue-chartjs'
import { shuffle } from 'lodash'

export default {
  extends: Doughnut,
  props: {
    operations: {
      type: Array,
      required: true,
    },
  },
  data () {
    return {
      dataset: this.operations.reduce((sums, { category, amount }) => {
        const sum = sums[category] || 0
        sums[category] = sum + Math.abs(amount)
        return sums
      }, {}),
    }
  },
  computed: {
    values () {
      return Object.values(this.dataset).map(it => Math.round(it))
    },
    labels () {
      return Object.keys(this.dataset)
    },
  },
  mounted () {
    const data = {
      datasets: [{
        data: this.values,
        backgroundColor: shuffle([
          'rgba(255, 99, 132, 0.7)',
          'rgba(54, 162, 235, 0.7)',
          'rgba(255, 206, 86, 0.7)',
          'rgba(75, 192, 192, 0.7)',
          'rgba(153, 102, 255, 0.7)',
          'rgba(255, 159, 64, 0.7)',
          'rgba(210, 145, 188, 0.7)',
          'rgba(128, 62, 21, 0.7)',
          'rgba(44, 71, 112, 0.7)',
          'rgba(35, 100, 103, 0.7)',
          'rgba(137, 152, 120, 0.7)',
          'rgba(228, 230, 195, 0.7)',
          'rgba(235, 101, 52, 0.7)',
          'rgba(47, 61, 96, 0.7)',
        ]),
      }],
      labels: this.labels,
    }

    this.renderChart(data, {
      responsive: true,
    })
  },
}
