import { Bar } from 'vue-chartjs'

export default {
  extends: Bar,
  props: {
    recettes: {
      type: Array,
      required: true,
    },
    depenses: {
      type: Array,
      required: true,
    },
  },
  mounted () {
    const data = {
      datasets: [{
        data: [
          Math.round(this.recettes.reduce((sum, { amount }) => sum + Math.abs(amount), 0)),
          Math.round(this.depenses.reduce((sum, { amount }) => sum + Math.abs(amount), 0)),
        ],
        backgroundColor: [
          'rgba(54, 162, 235, 0.7)',
          'rgba(153, 102, 255, 0.7)',
        ],
        yAxisID: 'y-axis-id',
      }],
      labels: ['Recettes', 'Dépenses'],
    }

    this.renderChart(data, {
      responsive: true,
      legend: {
        display: false,
      },
      scales: {
        yAxes: [{
          id: 'y-axis-id',
          type: 'linear',
          ticks: {
            beginAtZero: true,
          },
          position: 'left',
        }],
      },
    })
  },
}
