import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

import operations from './operations'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    operations,
  },
  plugins: [
    new VuexPersistence({
      storage: window.localStorage,
    }).plugin,
  ],
})
