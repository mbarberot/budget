import uuid from 'uuid/v4'

const operations = [
  {
    name: 'Pembina Pipeline Corp.',
    amount: -953.61,
    date: '2019-07-12',
    category: 'transports',
  },
  {
    name: 'Ciner Resources LP',
    amount: 2565.07,
    date: '2019-08-20',
    category: 'salaire',
  },
  {
    name: 'Countrywide Financial Corporation',
    amount: -272.1,
    date: '2019-08-29',
    category: 'transports',
  },
  {
    name: 'Avista Corporation',
    amount: -655.01,
    date: '2019-04-09',
    category: 'maison',
  },
  {
    name: 'Union Bankshares Corporation',
    amount: -184.82,
    date: '2019-01-07',
    category: 'loisirs',
  },
  {
    name: 'China Customer Relations Centers, Inc.',
    amount: -180.05,
    date: '2019-01-22',
    category: 'courses',
  },
  {
    name: 'Nymox Pharmaceutical Corporation',
    amount: -127.9,
    date: '2019-01-18',
    category: 'loisirs',
  },
  {
    name: 'Calamos Global Dynamic Income Fund',
    amount: 572.78,
    date: '2018-12-03',
    category: 'appartement',
  },
  {
    name: 'First Savings Financial Group, Inc.',
    amount: -505.32,
    date: '2019-03-04',
    category: 'maison',
  },
  {
    name: 'Web.com Group, Inc.',
    amount: -393.87,
    date: '2019-05-01',
    category: 'loisirs',
  },
  {
    name: 'XOMA Corporation',
    amount: -31.48,
    date: '2019-06-21',
    category: 'courses',
  },
  {
    name: 'National General Holdings Corp',
    amount: -74.75,
    date: '2018-10-11',
    category: 'maison',
  },
  {
    name: 'Citizens First Corporation',
    amount: -97.12,
    date: '2019-08-07',
    category: 'loisirs',
  },
  {
    name: 'Iteris, Inc.',
    amount: -19.52,
    date: '2019-03-12',
    category: 'courses',
  },
  {
    name: 'First Trust Small Cap Growth AlphaDEX Fund',
    amount: -795.12,
    date: '2019-07-13',
    category: 'maison',
  },
  {
    name: 'California Resources Corporation',
    amount: -719.55,
    date: '2018-12-25',
    category: 'courses',
  },
  {
    name: 'First Financial Bankshares, Inc.',
    amount: 529.85,
    date: '2019-09-25',
    category: 'appartement',
  },
  {
    name: 'Aceto Corporation',
    amount: -69.77,
    date: '2018-11-10',
    category: 'charges',
  },
  {
    name: 'Plumas Bancorp',
    amount: -591.2,
    date: '2019-01-29',
    category: 'loisirs',
  },
  {
    name: 'MainSource Financial Group, Inc.',
    amount: 549.7,
    date: '2019-03-07',
    category: 'appartement',
  },
  {
    name: 'Blue Buffalo Pet Products, Inc.',
    amount: -237.92,
    date: '2019-06-01',
    category: 'charges',
  },
  {
    name: 'Coty Inc.',
    amount: 1947.21,
    date: '2018-09-29',
    category: 'salaire',
  },
  {
    name: 'YRC Worldwide, Inc.',
    amount: 1811.12,
    date: '2019-04-09',
    category: 'salaire',
  },
  {
    name: 'Landmark Infrastructure Partners LP',
    amount: -466.15,
    date: '2019-09-05',
    category: 'transports',
  },
  {
    name: 'China Commercial Credit, Inc.',
    amount: -203.92,
    date: '2019-08-29',
    category: 'maison',
  },
  {
    name: 'Physicians Realty Trust',
    amount: -550.15,
    date: '2019-02-23',
    category: 'charges',
  },
  {
    name: 'ReWalk Robotics Ltd',
    amount: -366.58,
    date: '2019-01-26',
    category: 'transports',
  },
  {
    name: 'Electro-Sensors, Inc.',
    amount: -75.58,
    date: '2019-03-02',
    category: 'maison',
  },
  {
    name: 'Standard Motor Products, Inc.',
    amount: -445.11,
    date: '2019-01-31',
    category: 'loisirs',
  },
  {
    name: 'Bright Scholar Education Holdings Limited',
    amount: -146.05,
    date: '2018-12-29',
    category: 'courses',
  },
  {
    name: 'Spectrum Brands Holdings, Inc.',
    amount: -746.54,
    date: '2019-02-06',
    category: 'transports',
  },
  {
    name: 'Eagle Growth and Income Opportunities Fund',
    amount: 2928.19,
    date: '2019-06-17',
    category: 'salaire',
  },
  {
    name: 'Broadwind Energy, Inc.',
    amount: -357.0,
    date: '2018-12-16',
    category: 'loisirs',
  },
  {
    name: 'BroadSoft, Inc.',
    amount: -397.83,
    date: '2019-03-19',
    category: 'loisirs',
  },
  {
    name: 'Avadel Pharmaceuticals plc',
    amount: -935.51,
    date: '2019-04-01',
    category: 'transports',
  },
  {
    name: 'Vanguard Short-Term Corporate Bond ETF',
    amount: -940.3,
    date: '2019-04-08',
    category: 'loisirs',
  },
  {
    name: 'SolarEdge Technologies, Inc.',
    amount: -963.03,
    date: '2019-09-16',
    category: 'courses',
  },
  {
    name: 'SM Energy Company',
    amount: -670.4,
    date: '2018-11-29',
    category: 'courses',
  },
  {
    name: 'Schmitt Industries, Inc.',
    amount: -92.76,
    date: '2019-08-31',
    category: 'courses',
  },
  {
    name: 'Associated Banc-Corp',
    amount: 717.26,
    date: '2019-01-07',
    category: 'appartement',
  },
  {
    name: 'LightInTheBox Holding Co., Ltd.',
    amount: -879.07,
    date: '2019-05-02',
    category: 'maison',
  },
  {
    name: 'Rocky Brands, Inc.',
    amount: -530.42,
    date: '2019-07-24',
    category: 'charges',
  },
  {
    name: 'HSN, Inc.',
    amount: -578.59,
    date: '2018-10-18',
    category: 'loisirs',
  },
  {
    name: 'Markel Corporation',
    amount: 1906.54,
    date: '2019-08-23',
    category: 'salaire',
  },
  {
    name: 'Everbridge, Inc.',
    amount: 2617.53,
    date: '2019-04-18',
    category: 'salaire',
  },
  {
    name: 'Kelly Services, Inc.',
    amount: -53.07,
    date: '2019-02-18',
    category: 'transports',
  },
  {
    name: 'Kayne Anderson Midstream Energy Fund, Inc',
    amount: -38.52,
    date: '2019-06-21',
    category: 'transports',
  },
  {
    name: 'Chicago Bridge & Iron Company N.V.',
    amount: -79.17,
    date: '2018-11-10',
    category: 'maison',
  },
  {
    name: 'Allied Healthcare Products, Inc.',
    amount: -299.69,
    date: '2019-05-22',
    category: 'charges',
  },
  {
    name: 'Mesa Royalty Trust',
    amount: -145.61,
    date: '2019-05-23',
    category: 'transports',
  },
]

const state = {
  operations,
}

const getters = {
  getOperations: ({ operations }) => operations,
}

const mutations = {
  setOperations: (state, operations) => {
    state.operations = operations
  },
  setOperation: (state, operation) => {
    const index = state.operations.findIndex(({ id }) => id === operation.id)
    if (index >= 0) {
      state.operations.splice(index, 1, transform(operation))
    }
  },
  addOperations: (state, operations) => {
    state.operations = [...state.operations, ...operations.map(operation => transform(operation))]
  },
}

const actions = {}

const transform = ({ id, name, amount, category, date }) => ({
  id: id || uuid(),
  name,
  amount: Number(amount),
  category,
  date,
})

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true,
}

export {
  state,
  getters,
  mutations,
  actions,
}
