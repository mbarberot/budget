import { createLocalVue, mount } from '@vue/test-utils'
import Vue from 'vue'
import Vuex from 'vuex'
import Vuetify from 'vuetify'
import { getters, mutations, actions } from '@/store/operations'

import Operations from '@/components/budget/operations/Operations'

Vue.use(Vuetify)

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Vuetify)

function createStore (modules) {
  return new Vuex.Store({
    modules: { ...modules },
  })
}

function withOperations (operations) {
  return {
    operations: {
      namespaced: true,
      state: { operations: [...operations] },
      getters,
      mutations,
      actions,
    },
  }
}

describe('Operations', () => {
  let vuetify

  const createWrapper = (store, options) => {
    return mount(Operations, {
      localVue,
      store,
      vuetify,
      sync: false,
      ...options,
    })
  }

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  it('must display operations', () => {
    const wrapper = createWrapper(
      createStore(
        withOperations([{
          id: '1',
          name: 'Withdraw',
          amount: -30,
          date: '2019-09-01',
          category: 'misc',
        }, {
          id: '2',
          name: 'Lottery',
          amount: 1000000,
          date: '2015-07-04',
          category: 'luck',
        }])
      ),
      {
      },
    )

    expect(wrapper.findAll('[data-test-operation]').length).toEqual(2)
  })

  it('must display operations for a month and a year', () => {
    const wrapper = createWrapper(
      createStore(
        withOperations([{
          id: '1',
          name: 'Withdraw',
          amount: -30,
          date: '2019-09-01',
          category: 'misc',
        }, {
          id: '2',
          name: 'Lottery',
          amount: 1000000,
          date: '2015-07-04',
          category: 'luck',
        }])
      ),
      {
        propsData: {
          year: '2015',
          month: '07',
        },
      },
    )

    expect(wrapper.findAll('[data-test-operation]').length).toEqual(1)
  })
})
